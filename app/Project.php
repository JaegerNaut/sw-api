<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Project extends Model
{
    /**
     * @return HasMany
     */
    public function tags ()
    {
        return $this->hasMany(Tag::class);
    }
}
