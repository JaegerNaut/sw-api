<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Tag extends Model
{
    protected $fillable = [
        'name_en', 'name_fr', 'description_en', 'description_fr', 'project_id', 'article_id',
    ];

    /**
     * @return BelongsTo
     */
    public function article ()
    {
        return $this->belongsTo(Article::class);
    }

    /**
     * @return BelongsTo
     */
    public function project ()
    {
        return $this->belongsTo(Project::class);
    }
}
